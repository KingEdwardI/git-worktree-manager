#!/bin/bash

gwt__compile() {
  if [[ -e "$(pwd)/gwt" ]]; then
    rm ./gwt
  fi

  touch ./gwt
  { 
    cat ./help_menus.sh
    cat ./utils.sh
    cat ./git-worktree.sh
  } >> ./gwt
}

gwt__compile
chmod +x ./gwt
ln -nfs "$(pwd)"/gwt /usr/local/bin/gwt

if [[ "$*" =~ "--alias" ]]; then
  if ! [[ "$0" =~ "zsh" ]]; then
    echo "Shells other than zsh are not currently supported."
    echo "You'll have to manually add 'alias gwt=\". gwt\"' in your shell's rc file"
    exit 1
  fi
  if ! eval grep -q "alias gwt=\". gwt\"" "$HOME/.zshrc"; then
    echo "alias gwt=\". gwt\"" >> "$HOME/.zshrc"
  fi
fi


if [[ "$*" =~ "--autocomplete" ]]; then

  if ! [[ "$0" =~ "zsh" ]]; then
    echo "Autocomplete is not supported by shells other than zsh"
    exit 1
  fi

  completion_fpath=${ZSH_CUSTOM:-${ZSH:-$HOME/.oh-my-zsh}/custom}/plugins/zsh-completions

  if [[ ! -d "$completion_fpath" ]]; then
    echo "Cloning zsh-completions"
    git clone https://github.com/zsh-users/zsh-completions "$completion_fpath"
    echo "Adding zsh-completions to fpath... Updating .zshrc"
  fi

  if ! eval grep -q "$completion_fpath"; then
    cp "$HOME"/.zshrc "$HOME"/.gwt.zshrc.orig
    echo "$completion_fpath" >> "$HOME"/.zshrc
    cat "$HOME"/.zshrc.orig >> "$HOME"/.zshrc

    echo "Original zshrc file located at $HOME/.gwt.zshrc.orig"
    echo ".zshrc updated with fpath"
  fi


  echo "Linking autocomplete file"
  ln -nfs "$(pwd)"/_gwt.sh "$HOME"/.oh-my-zsh/custom/plugins/zsh-completions/src/_gwt
fi
