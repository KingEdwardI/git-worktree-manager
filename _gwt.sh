#!/bin/bash
#compdef gwt
#autoload

#shellcheck disable=SC2168
local -a available_worktrees
local -a state

#shellcheck disable=SC2034
args=(
  'help:Display the help menu'
  'add:Create a new worktree from the current working tree'
  'list:Print a list of worktrees for the current project'
  'switch:Switch between available worktrees'
  'move:Move an existing worktree to a new location'
  'remove:Delete an existing worktree'
  'repair:Repair target worktree, if possible'
  'prune:Prune worktree information'
  'lock:Lock the target worktree to prevent pruning, moving, or deleting'
  'unlock:Unlock the target worktree'
  'version:Print the current version number'
)

_arguments -C '*:: :->subcmds' && return 0

if (( CURRENT == 1 )); then
  _describe -t commands "gwt subcommand" args
  return
fi

_get_available_worktrees() {
  # Use echo here to flatten the list of refs
  #shellcheck disable=SC2005
  branch_refs=$(echo "$(git worktree list --porcelain | grep "branch")")
  branch_folders=${branch_refs//branch refs\/heads\///}
  branch_names=${branch_folders//\//}
  names=("${branch_names// / }")
  if (( ${#names} > 0 )); then
    for name in "${branch_names[@]}"; do
      available_worktrees="$available_worktrees $name"
    done
  fi
  echo "$available_worktrees"
}

goto_state_worktrees() {
  _arguments \
    '1: :->worktrees'
}

#shellcheck disable=SC2154
case "${words[1]}" in
  add|-a)
    _arguments \
      '(-f --force)'{-f,--force}'[checkout <branch> even if already checked out in other worktree]' \
      '(-b --branch)'{-b,--branch}'[create a new branch]' \
      '(-B --force-branch)'{-B,--force-branch}'[create or reset a branch]' \
      '(-d --detach)'{-d,--detach}'[detach HEAD at named commit]' \
      '(--checkout)--checkout[populate the new working tree]' \
      '(--lock)--lock[keep the new working tree locked]' \
      '(--reason)--reason[reason for locking]' \
      '(-q --quiet)'{-q,--quiet}'[suppress progress reporting]' \
      '(--track)--track[set up tracking mode (see git-branch(1))]' \
      '(--guess-remote)--guess-remote[try to match the new branch name with a remote-tracking branch]' \
      '(-s --switch)'{-s,--switch}'[if GWT_AUTO_SWITCH is turned off, switch branches when adding a worktree]'
    ;;
  list|-l)
    _arguments \
      '(--porcelain)--porcelain[machine-readable output]' \
      '(-v --verbose)'{-v,--verbose}'[show extended annotations and reasons, if available]' \
      '(--expire)--expire[add prunable annotation to worktrees older than <time>]' \
      '(-z)-z[terminate records with a NUL character]'
    ;;
  prune|-p)
    _arguments \
      '(-n --dry-run)'{-n,--dry-run}'[do not remove, show only]' \
      '(-v --verbose)'{-v,--verbose}'[report pruned working trees]' \
      '(--expire)--expire[expire working trees older than <time>]'
    ;;
  lock|-k|move|-m|remove|-r|switch|-s)
    goto_state_worktrees
      # Keeping these around for when I figure out how to re-implement them.
      # '(--reason)--reason[reason for locking - Note: multi-word reasons must be in quotes]' # lock
      # '(-f --force)'{-f,--force}'[force move even if worktree is dirty or locked]' # move
      # '(-f --force)'{-f,--force}'[force removal even if worktree is dirty or locked]' # remove
      # '(-b --branch)'{-b,--branch}'[delete the branch that was created with the worktree]' # remove
    ;;
esac

case "$state" in
  worktrees)
    wts=$(_get_available_worktrees)
    _arguments "1:worktrees:($wts)"
    ;;
  # *)
  #   compadd "$@"
  #   ;;
esac

