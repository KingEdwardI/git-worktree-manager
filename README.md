# Git Worktree Manager

This is a small script wrapper to help with managing git worktrees.

This provides a wrapper around the base commands, and some utility
methods to help with managing the worktrees.
Worktrees are stored at `$HOME/.worktrees` to avoid cluttering up local directories

## Installation

1. If needed, make the install script executable
    - `chmod +x ./install.sh`
2. Run the script `./install.sh`
    - Pass the argument `--alias` to automatically append `alias gwt=". gwt"` to your
      `.zshrc`.

<details>
<summary>Manual Installation</summary>

Most of this is just what the install script does, but this allows some customization

For now, the way I've gotten this to work is a bit convoluted.
So far it's solved most of the issues I've had, but it could be better.

1. If needed, make the script executable:
    - `chmod +x gwt`
2. Copy the script into your preferred bin folder.
    - `cp gwt /usr/bin/local/gwt`
    - **Note:** This is not required, you can put the script anywhere.
      I just find it convenient to have it in a static location.
3. Add an alias to your `.bashrc`, `.zshrc`, etc.
    - `alias gwt=". gwt"`
    - Alternatively (if you skip step 2): `alias gwt=". /path/to/gwt`
    - **Note** This is needed so that you actually change directories, rather than 
      changing directories in the sub-process

</details>

Now you should be good to go. Open a new shell and test it out by
simply running `gwt`. It should print the help menu.

### Custom Worktree directory

If you would like to override the default worktree directory, which is `$HOME/.worktrees`,
update your `.zshrc`, `bashrc`, etc with the following:

`GWT_WORKTREE_DIR="path/to/desired/location"`

### 

## Autocompletion

Autocompletion is currently only available for ZSH shells and uses [zsh-completions](https://github.com/zsh-users/zsh-completions).

### [oh-my-zsh](http://github.com/robbyrussell/oh-my-zsh)

- Clone zsh-completions into your oh-my-zsh repo:

```bash
  git clone https://github.com/zsh-users/zsh-completions ${ZSH_CUSTOM:-${ZSH:-~/.oh-my-zsh}/custom}/plugins/zsh-completions
```

- Add it to `FPATH` in your `.zshrc` by adding the following line before `source "$ZSH/oh-my-zsh.sh"`:

```bash
  fpath+=${ZSH_CUSTOM:-${ZSH:-~/.oh-my-zsh}/custom}/plugins/zsh-completions/src
```

- Run the install script with the `--autocomplete` argument

```bash
./install.sh --autocomplete
```

#### Background/Explanation

I originally wanted to write this in python or another language but
because those programs will run in their respective interpreters,
the `switch` command (the most important one) will not / cannot
change the directory of the shell you are working out of. 

The same effect is true with bash scripts. The script runs in a 
subshell, so no commands / actions are propagated back up to the
shell you are working out of.

So the workaround that I'm using is to `source` the script so
that it runs in the main shell and is able to switch folders there.
I also wanted to avoid having to execute a new shell once the directory
is changed, so there's not a stack of shells to exit out of.
Keep in mind that due to this, using `exit` commands will not work as
they will close the current shell if they get there.

## Usage

```
gwt - a wrapper for git's built-in worktree command.

This provides a wrapper around the base commands, and some utility
methods to help with managing the worktrees.
Worktrees are stored at \$HOME/.worktrees to avoid cluttering up local directories

Usage: gwt [options] [command] [args]

  gwt .............................................. displays this menu
  gwt [help -h] [<command>] ........................ displays this menu or the help menu for a specific command
  gwt [add -a] [<options>] <worktree-name> ......... creates a new worktree based on the current working tree
  gwt [list -l] [<options>] ........................ prints a list of worktrees for the current project
  gwt [switch -s] <worktree-name> .................. switches current worktrees
  gwt [move -m] [<options>] <old-name> <new_name> .. moves an existing worktree to a new location
  gwt [remove -r] [<options>] <worktree-name> ...... deletes an existing worktree
  gwt [repair fix -f] <worktree-name> .............. repair worktree, if possible
  gwt [prune -p] [<options>] ....................... prune worktree information
  gwt [lock -k] [<options>] <worktree-name> ........ locks the worktree to prevent pruning, moving, or deleting
  gwt [unlock -u] <worktree-name> .................. unlocks a given worktree
  gwt [version -v] ................................. prints the current version number

Run \`gwt help <command>\` for more information about specific commands
```

## Environment Variables

These environment variables can help customize `gwt` to your liking.

### GWT_AUTO_SWITCH

Automatically switch to the worktree that was just added.

Default value: `true`

### GWT_WORKTREE_DIR

Custom directory for worktrees to be stored in

Default value: `$HOME/.worktrees`

### GWT_AUTO_INSTALL_DEPS

Automatically install dependencies, if needed, when switching worktrees when turned on.

Default value: `true`

## Running Tests

- If needed, make the test files executable
  - `chmod +x ./test/*`
- Run the test script
  - `./test/test.sh`

### GWT_TEST_ENV

In order to make the program testable, an env variable is turned on when the tests run so 
the code takes a slightly different path. This isn't the best of practices, but so far 
it's the best I've got.

A good example is for the `gwt remove --branch foobar`, which asks for user confirmation.
In this case we don't want to have to interact with the test. It acts sort of like an
in-code mocking system.

## Contributing

Before submitting a PR, install/compile the `gwt` file and use `[shellcheck](https://www.shellcheck.net/)`
to ensure that the additions are compliant. There are also editor plugins that can help with this.

## Todo:

See this [issue](https://gitlab.com/KingEdwardI/git-worktree-manager/-/issues/1)

## Credit

This is heavily based off of [git-worktree-switcher](https://github.com/yankeexe/git-worktree-switcher). 
I wanted a better general experience, with the `git worktree` commands
built in and consistent usage as far as `gwt <option> <command> <argument>`.
