#!/bin/bash
# shellcheck disable=SC2154

repair_test () {
  gwt__repair
  echo "$gwt__repair_cmd"
}
describe "gwt repair"

  test=$(it "Produces the correct command")
  actual=$(repair_test)
  assertEquals "$actual" "git worktree repair" "$test"
