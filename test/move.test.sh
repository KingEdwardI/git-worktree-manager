#!/bin/bash
# shellcheck disable=SC2154

move_test() {
  gwt__move "$@"
  echo "$gwt__move_cmd"
}
describe "gwt move"

  test=$(it "Throws if there are not enough args")
  actual=$(move_test)
  expected="gwt__not_enough_args_error gwt__move_help_menu"
  assertEquals "$actual" "$expected" "$test"

  test=$(it "Throws if the --force option is provided but worktree names are not")
  actual=$(move_test "--force")
  expected="gwt__not_enough_args_error gwt__move_help_menu"
  assertEquals "$actual" "$expected" "$test"

  test=$(it "Throws if the --force option is provided but only one worktree name is not")
  actual=$(move_test "--force" "old-tree")
  expected="gwt__not_enough_args_error gwt__move_help_menu"
  assertEquals "$actual" "$expected" "$test"

  test=$(it "Throws if only one worktree name is provided")
  actual=$(move_test "old-tree")
  expected="gwt__not_enough_args_error gwt__move_help_menu"
  assertEquals "$actual" "$expected" "$test"

  test=$(it "Produces the correct result when both worktree names are included")
  actual=$(move_test "old-tree" "new-tree")
  assertEquals "$actual" "git worktree move ${gwt__worktree_dir}/old-tree ${gwt__worktree_dir}/new-tree" "$test"

  test=$(it "Produces the correct result when --force option and both worktree names are included")
  actual=$(move_test "--force" "old-tree" "new-tree")
  assertEquals "$actual" "git worktree move -f -f ${gwt__worktree_dir}/old-tree ${gwt__worktree_dir}/new-tree" "$test"

  test=$(it "Produces the correct result when -f option and both worktree names are included")
  actual=$(move_test "--force" "old-tree" "new-tree")
  assertEquals "$actual" "git worktree move -f -f ${gwt__worktree_dir}/old-tree ${gwt__worktree_dir}/new-tree" "$test"
