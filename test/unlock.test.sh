#!/bin/bash
# shellcheck disable=SC2154

unlock_test() {
  gwt__unlock "$@"
  echo "$gwt__unlock_cmd"
}
describe "gwt unlock"

  test=$(it "Throws an error when no arguments are provided")
  actual=$(unlock_test)
  expected="gwt__not_enough_args_error gwt__unlock_help_menu"
  assertEquals "$actual" "$expected" "$test"

  test=$(it "Produces the correct command when a worktree name is provided")
  actual=$(unlock_test "foobar")
  assertEquals "$actual" "git worktree unlock foobar" "$test"
