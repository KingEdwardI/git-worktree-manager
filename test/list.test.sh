#!/bin/bash
# shellcheck disable=SC2154

list_test() {
  gwt__list "$@"
  echo "$gwt__list_cmd"
}
describe "gwt list - basic commands"

  opts=("--porcelain" "--verbose" "-z")

  test=$(it "Produces the correct default command")
  actual=$(list_test)
  assertEquals "$actual" "git worktree list" "$test"

  for opt in "${opts[@]}"; do
    test=$(it "Produces the correct command with $opt")
    actual=$(list_test "$opt")
    assertEquals "$actual" "git worktree list $opt" "$test"
  done

  test=$(it "Produces the correct command with -v")
  actual=$(list_test "-v")
  assertEquals "$actual" "git worktree list --verbose" "$test"

  test=$(it "Produces the correct command with --expire")
  actual=$(list_test "--expire" "123456")
  assertEquals "$actual" "git worktree list --expire 123456" "$test"

  test=$(it "Throws an error when no argument is provided to --expire")
  actual=$(list_test "--expire")
  expected="gwt__not_enough_args_error gwt__list_help_menu"
  assertEquals "$actual" "$expected" "$test"
