#!/bin/bash
# shellcheck disable=SC2154,SC2034

switch_test() {
  gwt__switch "$@"
  echo "$gwt__switch_cmd"
}
describe "gwt switch"

  test=$(it "Produces the correct command when no arguments are passed")
  actual=$(switch_test)
  assertEquals "$actual" "cd $(pwd)" "$test"

  test=$(it "Produces the correct command when \`-\` is passed as an arg")
  actual=$(switch_test "-")
  assertEquals "$actual" "cd $(pwd)" "$test"

  test=$(it "Produces the correct command when a worktree is passed")
  actual=$(switch_test "foobar")
  assertEquals "$actual" "cd ${gwt__worktree_dir}/foobar && gwt__install_deps_if_needed" "$test"

  test=$(it "Produces the correct command when a worktree is passed and GWT_AUTO_INSTALL_DEPS is off")
  GWT_AUTO_INSTALL_DEPS="false"
  actual=$(switch_test "foobar")
  assertEquals "$actual" "cd ${gwt__worktree_dir}/foobar" "$test"
  GWT_AUTO_INSTALL_DEPS="true"

describe "gwt switch - errors"

  test=$(it "Throws an error when not in a git repo")
  current_dir=$(pwd)
  mkdir "$HOME/gwt_test_dir"
  cd "$HOME/gwt_test_dir" || echo "Fatal: gwt switch test failed, test directory doesn't exist"
  actual=$(switch_test)
  assertEquals "$actual" "gwt__not_a_git_repo_error_message" "$test"
  cd  "$current_dir" || echo "Fatal: gwt switch test failed, gwt directory doesn't exist"
  rm -rf "$HOME/gwt_test_dir"
