#!/bin/bash
# shellcheck disable=SC2154

prune_test() {
  gwt__prune "$@"
  echo "$gwt__prune_cmd"
}
describe "gwt prune"

  test=$(it "Produces the correct command by default")
  actual=$(prune_test)
  assertEquals "$actual" "git worktree prune " "$test"

  test=$(it "Produces the correct result with all long options included")
  actual=$(prune_test "--dry-run" "--verbose" "--expire" "foobar")
  assertEquals "$actual" "git worktree prune  --dry-run --verbose --expire foobar" "$test"

  test=$(it "Throws an error when unknown options are provided")
  actual=$(prune_test "--blah")
  expected="gwt__unknown_args_error prune_help_menu --blah"
  assertEquals "$actual" "$expected" "$test"
