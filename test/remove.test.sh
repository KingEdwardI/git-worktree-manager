#!/bin/bash
# shellcheck disable=SC2154

remove_test() {
  gwt__remove "$@"
  echo "$gwt__remove_cmd"
}
describe "gwt remove"

  test=$(it "Produces the correct command with a worktree name")
  actual=$(remove_test "foobar")
  assertEquals "$actual" "git worktree remove  foobar" "$test"

  test=$(it "Produces the correct command with -f and a worktree name")
  actual=$(remove_test "-f" "foobar")
  assertEquals "$actual" "git worktree remove  -f -f foobar" "$test"

  test=$(it "Produces the correct command with --force and a worktree name")
  actual=$(remove_test "--force" "foobar")
  assertEquals "$actual" "git worktree remove  -f -f foobar" "$test"

  test=$(it "Produces the correct command with --branch and a worktree name")
  # echo will pipe the response into the remove method and simulate user input this way.
  actual=$(echo "yes" | remove_test "--branch" "foobar")
  assertEquals "$actual" "git worktree remove  foobar && git branch -D foobar" "$test"

  test=$(it "Produces the correct command with --branch, --force, and a worktree name")
  # echo will pipe the response into the remove method and simulate user input this way.
  actual=$(echo "yes" | remove_test "--branch" "--force" "foobar")
  assertEquals "$actual" "git worktree remove  -f -f foobar && git branch -D foobar" "$test"

describe "gwt remove - errors"

  test=$(it "Throws an error when no arguments are provided")
  actual=$(remove_test)
  expected="gwt__not_enough_args_error gwt__remove_help_menu"
  assertEquals "$actual" "$expected" "$test"

  test=$(it "Throws an error when --force is provided without a worktree name")
  actual=$(remove_test "--force")
  expected="gwt__not_enough_args_error gwt__remove_help_menu"
  assertEquals "$actual" "$expected" "$test"

  test=$(it "Throws an error when -f is provided without a worktree name")
  actual=$(remove_test "-f")
  expected="gwt__not_enough_args_error gwt__remove_help_menu"
  assertEquals "$actual" "$expected" "$test"

  test=$(it "Throws an error when --branch is provided without a worktree name")
  actual=$(remove_test "--branch")
  expected="gwt__not_enough_args_error gwt__remove_help_menu"
  assertEquals "$actual" "$expected" "$test"

  test=$(it "Throws an error when -b is provided without a worktree name")
  actual=$(remove_test "-b")
  expected="gwt__not_enough_args_error gwt__remove_help_menu"
  assertEquals "$actual" "$expected" "$test"

  test=$(it "Throws an error when --force and --branch are provided without a worktree name")
  actual=$(remove_test "--force" "--branch")
  expected="gwt__not_enough_args_error gwt__remove_help_menu"
  assertEquals "$actual" "$expected" "$test"

  test=$(it "Throws an error when an unknown command is provided")
  actual=$(remove_test "--blah")
  expected="gwt__unknown_args_error gwt__remove_help_menu --blah"
  assertEquals "$actual" "$expected" "$test"

