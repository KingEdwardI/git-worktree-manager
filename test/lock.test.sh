#!/bin/bash
# shellcheck disable=SC2154

lock_test() {
  gwt__lock "$@"
  echo "$gwt__lock_cmd"
}
describe "gwt lock"
  
  test=$(it "Throws an error if there aren't enough arguments")
  actual=$(lock_test)
  expected="gwt__not_enough_args_error gwt__lock_help_menu"
  assertEquals "$actual" "$expected" "$test"

  test=$(it "Throws an error if --reason is provided but a worktree name is not")
  actual=$(lock_test "--reason" "foobar")
  expected="gwt__not_enough_args_error gwt__lock_help_menu"
  assertEquals "$actual" "$expected" "$test"

  test=$(it "Produces the correct command with default args")
  actual=$(lock_test "foobar")
  assertEquals "$actual" "git worktree lock ${gwt__worktree_dir}/foobar" "$test"

  test=$(it "Produces the correct command with reason included")
  reason="a reasonable reason" 
  actual=$(lock_test "--reason" "$reason" "foobar")
  assertEquals "$actual" "git worktree lock --reason '$reason' ${gwt__worktree_dir}/foobar" "$test"
