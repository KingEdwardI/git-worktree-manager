#!/bin/bash
# shellcheck disable=SC2154,SC1091

export GWT_TEST_ENV="true"

./install.sh

. './test/utils.sh'
. './gwt'

. './test/add.test.sh'
. './test/list.test.sh'
. './test/lock.test.sh'
. './test/move.test.sh'
. './test/prune.test.sh'
. './test/remove.test.sh'
. './test/repair.test.sh'
. './test/switch.test.sh'
. './test/unlock.test.sh'

echo ""
echo -e "${green}Tests passed:${reset} $passed"
echo -e "${red}Tests failed:${reset} $failed"

if [[ $failed -gt 0 ]]; then
  exit 1
else
  exit 0
fi
