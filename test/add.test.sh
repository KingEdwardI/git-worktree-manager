#!/bin/bash
# shellcheck disable=SC2154,SC2034

add_test() {
  gwt__add "$@"
  echo "$gwt__add_cmd"
}

describe "gwt add - basic commands"

  opts=("--force" "--checkout" "--lock" "--quiet" "--track" "--guess-remote")

  test=$(it "Produces the correct command with only a worktree name")
  actual=$(add_test "foobar")
  assertEquals "$actual" "git worktree add  ${gwt__worktree_dir}/foobar; gwt switch foobar" "$test"

  test=$(it "Produces the correct command with -b branch")
  actual=$(add_test "-b" "branch" "foobar")
  assertEquals "$actual" "git worktree add  -b branch ${gwt__worktree_dir}/foobar; gwt switch foobar" "$test"

  test=$(it "Produces the correct command with -B branch")
  actual=$(add_test "-B" "branch" "foobar")
  assertEquals "$actual" "git worktree add  -B branch ${gwt__worktree_dir}/foobar; gwt switch foobar" "$test"

  test=$(it "Produces the correct command with -d")
  actual=$(add_test "-d" "foobar")
  assertEquals "$actual" "git worktree add  --detach ${gwt__worktree_dir}/foobar; gwt switch foobar" "$test"

  test=$(it "Produces the correct command with --detach")
  actual=$(add_test "--detach" "foobar")
  assertEquals "$actual" "git worktree add  --detach ${gwt__worktree_dir}/foobar; gwt switch foobar" "$test"

  for opt in "${opts[@]}"; do
    test=$(it "Produces the correct command with $opt")
    actual=$(add_test "$opt" "foobar")
    assertEquals "$actual" "git worktree add  $opt ${gwt__worktree_dir}/foobar; gwt switch foobar" "$test"
  done

  test=$(it "Produces the correct command when --lock is provided before --reason")
  reason="many reasons" 
  actual=$(add_test "--lock" "--reason" "$reason" "foobar")
  assertEquals "$actual" "git worktree add  --lock --reason '$reason' ${gwt__worktree_dir}/foobar; gwt switch foobar" "$test"

  test=$(it "Produces the correct command when --reason is provided before --lock")
  reason="many reasons" 
  actual=$(add_test "--reason" "$reason" "--lock" "foobar")
  assertEquals "$actual" "git worktree add  --reason 'many reasons' --lock ${gwt__worktree_dir}/foobar; gwt switch foobar" "$test"

  GWT_AUTO_SWITCH=""
  test=$(it "Produces the correct command with -s when GWT_AUTO_SWITCH is not set")
  actual=$(add_test "-s" "foobar")
  assertEquals "$actual" "git worktree add  ${gwt__worktree_dir}/foobar" "$test"

  GWT_AUTO_SWITCH=""
  test=$(it "Produces the correct command with --switch when GWT_AUTO_SWITCH is not set")
  actual=$(add_test "--switch" "foobar")
  assertEquals "$actual" "git worktree add  ${gwt__worktree_dir}/foobar" "$test"

  GWT_AUTO_SWITCH="true"
  test=$(it "Produces the correct command with -s when GWT_AUTO_SWITCH is set to true")
  actual=$(add_test "-s" "foobar")
  assertEquals "$actual" "git worktree add  ${gwt__worktree_dir}/foobar" "$test"

  GWT_AUTO_SWITCH="true"
  test=$(it "Produces the correct command with --switch when GWT_AUTO_SWITCH is set to true")
  actual=$(add_test "--switch" "foobar")
  assertEquals "$actual" "git worktree add  ${gwt__worktree_dir}/foobar" "$test"

  GWT_AUTO_SWITCH="false"
  test=$(it "Produces the correct command with -s when GWT_AUTO_SWITCH is set to false")
  actual=$(add_test "-s" "foobar")
  assertEquals "$actual" "git worktree add  ${gwt__worktree_dir}/foobar; gwt switch foobar" "$test"

  GWT_AUTO_SWITCH="false"
  test=$(it "Produces the correct command with --switch when GWT_AUTO_SWITCH is set to false")
  actual=$(add_test "--switch" "foobar")
  assertEquals "$actual" "git worktree add  ${gwt__worktree_dir}/foobar; gwt switch foobar" "$test"
  # Make sure to un-set this for the rest of the tests
  GWT_AUTO_SWITCH=""

  test=$(it "Produces the correct command when a hash is provided")
  actual=$(add_test "foobar" "hash")
  assertEquals "$actual" "git worktree add  ${gwt__worktree_dir}/foobar hash; gwt switch foobar" "$test"

  for opt in "${opts[@]}"; do
    test=$(it "Produces the correct command with $opt and a hash")
    actual=$(add_test "$opt" "foobar" "hash")
    assertEquals "$actual" "git worktree add  $opt ${gwt__worktree_dir}/foobar hash; gwt switch foobar" "$test"
  done

describe "gwt add - basic commands throw errors correctly"

  test=$(it "Throws error when unknown args are provided")
  actual=$(add_test "--foobar")
  assertEquals "$actual" "gwt__unknown_args_error gwt__add_help_menu --foobar" "$test"
  actual=$(add_test "-q")
  assertEquals "$actual" "gwt__unknown_args_error gwt__add_help_menu -q" "$test"

  test=$(it "Throws an error when --reason is provided without --lock")
  actual=$(add_test "--reason" "foobar")
  assertEquals "$actual" "gwt__invalid_use_of_reason_error_message" "$test"

  test=$(it "Throws error when no args are provided")
  actual=$(add_test)
  expected="gwt__not_enough_args_error gwt__add_help_menu"
  assertEquals "$actual" "$expected" "$test"

describe "gwt add - options '-b', '-B', and '-d' throw errors correctly"

  test=$(it "Throws error when no workflow name is provided to -b")
  actual=$(add_test "-b" "branch")
  assertEquals "$actual" "gwt__not_enough_args_error" "$test"

  test=$(it "Throws error when no workflow name is provided to -B")
  actual=$(add_test "-B" "foobar")
  assertEquals "$actual" "gwt__not_enough_args_error" "$test"

  test=$(it "Throws error when -b and -B flags are both included in that order")
  actual=$(add_test "-b" "branch" "-B" "branch" "foobar")
  assertEquals "$actual" "gwt__invalid_arg_combo_error_message" "$test"

  test=$(it "Throws error when -b and -d flags are both included in that order")
  actual=$(add_test "-b" "branch" "-d" "foobar")
  assertEquals "$actual" "gwt__invalid_arg_combo_error_message" "$test"

  test=$(it "Throws error when -B and -b flags are both included in that order")
  actual=$(add_test "-B" "branch" "-b" "branch" "foobar")
  assertEquals "$actual" "gwt__invalid_arg_combo_error_message" "$test"

  test=$(it "Throws error when -B and -d flags are both included in that order")
  actual=$(add_test "-B" "branch" "-d" "foobar")
  assertEquals "$actual" "gwt__invalid_arg_combo_error_message" "$test"

  test=$(it "Throws error when -d and -b flags are both included in that order")
  actual=$(add_test "-d" "-b" "branch" "foobar")
  assertEquals "$actual" "gwt__invalid_arg_combo_error_message" "$test"

  test=$(it "Throws error when -d and -B flags are both included in that order")
  actual=$(add_test "-d" "-B" "branch" "foobar")
  assertEquals "$actual" "gwt__invalid_arg_combo_error_message" "$test"

  test=$(it "Throws error when -b, -B, and -d flags are all included in that order")
  actual=$(add_test "-b" "branch" "-B" "branch" "-d" "foobar")
  assertEquals "$actual" "gwt__invalid_arg_combo_error_message" "$test"

  test=$(it "Throws error when -b, -d, and -B flags are all included in that order")
  actual=$(add_test "-b" "branch" "-d" "-B" "branch" "foobar")
  assertEquals "$actual" "gwt__invalid_arg_combo_error_message" "$test"

  test=$(it "Throws error when -B, -b, and -d flags are all included in that order")
  actual=$(add_test "-B" "branch" "-b" "branch" "-d" "foobar")
  assertEquals "$actual" "gwt__invalid_arg_combo_error_message" "$test"

  test=$(it "Throws error when -B, -d, and -b flags are all included in that order")
  actual=$(add_test "-B" "branch" "-d" "-b" "branch" "foobar")
  assertEquals "$actual" "gwt__invalid_arg_combo_error_message" "$test"

  test=$(it "Throws error when -d, -b, and -B flags are all included in that order")
  actual=$(add_test "-d" "-b" "branch" "-B" "branch" "foobar")
  assertEquals "$actual" "gwt__invalid_arg_combo_error_message" "$test"

  test=$(it "Throws error when -d, -B, and -b flags are all included in that order")
  actual=$(add_test "-d" "-B" "branch" "-b" "branch" "foobar")
  assertEquals "$actual" "gwt__invalid_arg_combo_error_message" "$test"
