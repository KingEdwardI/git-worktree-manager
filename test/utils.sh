#!/bin/bash

reset='\033[0m'
red='\033[0;31m'
green='\033[0;32m'
purple='\033[0;35m'
cyan='\033[0;36m'
yellow='\033[0;33m'

passed=0
failed=0

# Todo: replace echo with printf (probably) - it doesn't produce colors when running from each specific file

describe() {
  test_suite=$1
  echo ""
  echo -e "${purple}> ${test_suite}${reset}"
}

it() {
  test_case="$1"
  echo -e "${cyan}It ${test_case}${reset}"
}

todo() {
  # Todo: a better color for this, preferably yellow background & black text
  echo -e "    ${yellow}TODO: $1${reset}"
}

pass() {
  echo -e "    ${green}PASS:${reset} ${test_case}"
  passed=$((passed + 1))
}

fail() {
  echo -e "    ${red}FAIL:${reset} ${test_case}"
  failed=$((failed + 1))
}

result () {
  actual=$1
  expected=$2

  echo -e "    ${yellow}> Actual:${reset}"
  echo -e "      \"$actual\""
  echo -e "    ${yellow}> Expected:${reset}"
  echo -e "      \"$expected\""
}

assertEquals() {
  actual=$1
  expected=$2
  test_case=$3

  if [[ -z $3 ]]; then
    test_case="${yellow}No test name provided${reset}"
  fi

  if [[ "$actual" == "$expected" ]]; then
    pass "$test_case"
  else
    fail "$test_case"
    result "$actual" "$expected"
  fi
}

# describe "Testing test suite"
#   test=$(it "works correctly")
#   assertEqual "foo" "foo" "$test"

#   test=$(it "fails correctly")
#   assertEqual "foo" "bar" "$test"
