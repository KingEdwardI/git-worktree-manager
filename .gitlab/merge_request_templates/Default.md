## Checklist

- [ ] Added tests
- [ ] Updated Readme
- [ ] Updated help menus
- [ ] Updated autocompletion
- [ ] Ran scripts through [Shellcheck](https://www.shellcheck.net/)

## Changes
