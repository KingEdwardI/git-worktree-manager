#!/bin/bash

gwt__help() {
  if [[ (-z "$1") || ("$1" == "-h") ]]; then
    echo "gwt - a wrapper for git's built-in worktree command."
    echo ""
    echo "This provides a wrapper around the base commands, and some utility"
    echo "methods to help with managing the worktrees."
    echo "Worktrees are stored at \$HOME/.worktrees to avoid cluttering up local directories"
    echo ""
    echo "Usage: gwt [command] [args]"
    echo ""
    echo "  gwt .............................................. displays this menu"
    echo "  gwt [help -h] .................................... displays this menu"
    echo "  gwt [add -a] [<options>] <worktree-name> ......... creates a new worktree based on the current working tree"
    echo "  gwt [list -l] [<options>] ........................ prints a list of worktrees for the current project"
    echo "  gwt [switch -s] <worktree-name> .................. switches current worktrees"
    echo "  gwt [move -m] [<options>] <old-name> <new_name> .. moves an existing worktree to a new location"
    echo "  gwt [remove -r] [<options>] <worktree-name> ...... deletes an existing worktree"
    echo "  gwt [repair fix -f] <worktree-name> .............. repair worktree, if possible"
    echo "  gwt [prune -p] [<options>] ....................... prune worktree information"
    echo "  gwt [lock -k] [<options>] <worktree-name> ........ locks the worktree to prevent pruning, moving, or deleting"
    echo "  gwt [unlock -u] <worktree-name> .................. unlocks a given worktree"
    echo ""
    echo "Run \`gwt help <command>\` for more information about specific commands"
  elif [[ "$1" == "add" ]]; then
    gwt__add_help_menu
  elif [[ "$1" == "list" ]]; then
    gwt__list_help_menu
  elif [[ "$1" == "lock" ]]; then
    gwt__lock_help_menu
  elif [[ "$1" == "move" ]]; then
    gwt__move_help_menu
  elif [[ "$1" == "prune" ]]; then
    prune_help_menu
  elif [[ "$1" == "remove" ]]; then
    gwt__remove_help_menu
  elif [[ "$1" == "repair" ]]; then
    gwt__repair_help_menu
  elif [[ "$1" == "unlock" ]]; then
    gwt__unlock_help_menu
  elif [[ "$1" == "switch" ]]; then
    gwt__switch_help_menu
  else
    echo "Error: Unknown option(s) $*"
  fi
}

gwt__add_help_menu() {
  echo "Usage: gwt add [<options>] <worktree> [<commit-ish>]"
  echo ""
  echo "  -f, --force           checkout <branch> even if already checked out in other worktree"
  echo "  -b <branch>           create a new branch"
  echo "  -B <branch>           create or reset a branch"
  echo "  -d, --detach          detach HEAD at named commit"
  echo "  --checkout            populate the new working tree"
  echo "  --lock                keep the new working tree locked"
  echo "  --reason <string>     reason for locking"
  echo "  -q, --quiet           suppress progress reporting"
  echo "  --track               set up tracking mode (see git-branch(1))"
  echo "  --guess-remote        try to match the new branch name with a remote-tracking branch"
  echo "  -s, --switch          if GWT_AUTO_SWITCH is turned off, switch branches when adding a worktree"
}

gwt__list_help_menu() {
  echo "Usage: gwt list [<options>]"
  echo ""
  echo "  --porcelain             machine-readable output"
  echo "  -v, --verbose           show extended annotations and reasons, if available"
  echo "  --expire <expiry-date>  add 'prunable' annotation to worktrees older than <time>"
  echo "  -z                      terminate records with a NUL character"
}

gwt__lock_help_menu() {
  echo "Usage: gwt lock [<options>] <worktree>"
  echo ""
  echo "  --reason <string>          reason for locking - Note: multi-word reasons must be in quotes"
}

gwt__move_help_menu() {
  echo "Usage: gwt move [<options>] <worktree> <new-path>"
  echo ""
  echo "  -f, --force          force move even if worktree is dirty or locked"
}

prune_help_menu() {
  echo "Usage: gwt prune [<options>]"
  echo ""
  echo "  -n, --dry-run           do not remove, show only"
  echo "  -v, --verbose           report pruned working trees"
  echo "  --expire <expiry-date>  expire working trees older than <time>"
}

gwt__remove_help_menu() {
  echo "Usage: gwt remove [<options>] <worktree>"
  echo ""
  echo "  -f, --force            force removal even if worktree is dirty or locked"
  echo "  -b, --branch           delete the branch that was created with the worktree"
}

gwt__repair_help_menu() {
  echo "Usage: gwt repair <worktree-name>"
}

gwt__unlock_help_menu() {
  echo "Usage: gwt unlock <worktree-name>"
}

gwt__switch_help_menu() {
  echo "Switch between worktrees. By not passing a worktree name, or by passing \`-\`"
  echo "  it will switch to the root worktree (the main repository)."
  echo ""
  echo "Usage: gwt switch [<options>] [<worktree-name>]"
}
