#!/bin/bash

GWT_VERSION="0.10.0"

if [[ -z "$GWT_WORKTREE_DIR" ]]; then
  gwt__project_name=$(git remote -v  | sed -rn '1s#.*/(.*)\.git.*#\1#p')
  gwt__worktree_dir="$HOME/.worktrees/$gwt__project_name"
else
  if [[ ! -d "$GWT_WORKTREE_DIR" ]]; then
    echo "Custom worktree directory doesn't exist, creating it now..."
    mkdir -p "$GWT_WORKTREE_DIR"
  fi
  gwt__worktree_dir="$GWT_WORKTREE_DIR"
fi

## Base Methods ##

gwt__add_cmd=""
gwt__add() {
  # options_for_add=("--force" "-f" "-b" "-B" "--detatch" "-d" "--checkout" "--lock" "--reason")
  # echo $(array_includes options_for_add "--force")
  if [[ -z $* ]]; then
    gwt__add_cmd="gwt__not_enough_args_error gwt__add_help_menu"
    return
  fi

  gwt__command="git worktree add"
  gwt__opts=""
  if [[ "$GWT_AUTO_SWITCH" == "true" || -z "$GWT_AUTO_SWITCH" ]]; then
    gwt__should_switch_wt=true
  else
    gwt__should_switch_wt=false
  fi

  while [ $# -gt 0 ]; do
    case $1 in
      --force|-f)
        gwt__opts="${gwt__opts} --force"
        shift
        ;;
      -b)
        if [[ ($* =~ "-B") || ($* =~ "-d") || ($* =~ "--detach") ]]; then
          gwt__add_cmd="gwt__invalid_arg_combo_error_message"
          return
        fi

        gwt__opts="${gwt__opts} -b $2"
        shift
        shift
        if [[ $# == 0 ]]; then
          gwt__add_cmd="gwt__not_enough_args_error"
          return
        fi
        ;;
      -B)
        # Todo: ensure that $2 is a branch name and not something else... but how?
        if [[ ($* =~ "-b") || ($* =~ "-d") || ($* =~ "--detach") ]]; then
          gwt__add_cmd="gwt__invalid_arg_combo_error_message"
          return
        fi

        gwt__opts="${gwt__opts} -B $2"
        shift
        shift
        if [[ $# == 0 ]]; then
          gwt__add_cmd="gwt__not_enough_args_error"
          return
        fi
        ;;
      --detach|-d)
        if [[ ($* =~ "-b") || ($* =~ "-B") ]]; then
          gwt__add_cmd="gwt__invalid_arg_combo_error_message"
          return
        fi

        gwt__opts="${gwt__opts} --detach"
        shift
        ;;
      --checkout)
        gwt__opts="${gwt__opts} --checkout"
        shift
        ;;
      --lock)
        gwt__opts="${gwt__opts} --lock"
        shift
        ;;
      --reason)
        if [[ ($GWT_TEST_ENV == false) || (-z $GWT_TEST_ENV) ]]; then
          eval gwt__reason="$2"
        else
          gwt__reason="$2"
        fi

        if ! [[ ($* =~ "--lock") || ("$gwt__opts" =~ "--lock") ]]; then
          gwt__add_cmd="gwt__invalid_use_of_reason_error_message"
          return
        fi
        gwt__opts="${gwt__opts} --reason '$gwt__reason'"
        shift
        shift
        ;;
      --quiet)
        gwt__opts="${gwt__opts} --quiet"
        shift
        ;;
      --track)
        gwt__opts="${gwt__opts} --track"
        shift
        ;;
      --guess-remote)
        gwt__opts="${gwt__opts} --guess-remote"
        shift
        ;;
      --switch|-s)
        if [[ "$GWT_AUTO_SWITCH" == "true" || -z "$GWT_AUTO_SWITCH" ]]; then
          gwt__should_switch_wt=false
        else
          gwt__should_switch_wt=true
        fi
        shift
        ;;
      *)
        if [[ $# -lt 3 ]]; then
          break
        fi
        gwt__add_cmd="gwt__unknown_args_error gwt__add_help_menu $*"
        return
        ;;
    esac
  done

  if [[ "$1" = -* || "$2" = -* ]]; then
    gwt__add_cmd="gwt__unknown_args_error gwt__add_help_menu $*"
    return
  fi


  if [[ $# == 2 ]]; then
    gwt__wt="$1"
    gwt__hash="$2"
    shift 
    shift
    gwt__command="${gwt__command} ${gwt__opts} ${gwt__worktree_dir}/${gwt__wt} ${gwt__hash}"
  elif [[ $# == 1 ]]; then
    gwt__wt="$1"
    shift
    gwt__command="${gwt__command} ${gwt__opts} ${gwt__worktree_dir}/${gwt__wt}"
  fi

  if [[ $gwt__should_switch_wt == true ]]; then
    gwt__command="${gwt__command}; gwt switch ${gwt__wt}"
  fi


  gwt__add_cmd="$gwt__command"
}

gwt__list_cmd=""
gwt__list() {
  gwt__command="git worktree list"
  gwt__opts=""
  while [ $# -gt 0 ]; do
    case $1 in
      --porcelain)
        gwt__command="${gwt__command} --porcelain"
        shift
        ;;
      --verbose|-v)
        gwt__command="${gwt__command} --verbose"
        shift
        ;;
      --expire)
        if [[ -z $2 ]]; then
          gwt__list_cmd="gwt__not_enough_args_error gwt__list_help_menu"
          return
        fi

        gwt__command="${gwt__command} --expire $2"
        shift
        shift
        ;;
      -z)
        gwt__command="${gwt__command} -z"
        shift
        ;;
      *)
        gwt__list_cmd="gwt__unknown_args_error gwt__list_help_menu $*"
        return
        ;;
    esac
  done

  gwt__list_cmd="$gwt__command"
}

gwt__lock_cmd=""
gwt__lock() {
  if [[ -z $* ]]; then
    gwt__lock_cmd="gwt__not_enough_args_error gwt__lock_help_menu"
    return
  fi

  gwt__command="git worktree lock"
  gwt__opts=""

  # Todo: somehow ensure that $2 is not the branch name?
  # check for --reason and count args from there
  if [[ "$1" == "--reason" ]]; then
    # Have to do this stupid hack so that the test behaves correctly and the actual command does as well
    # When using `eval`, it can take an escaped interpolation and figure out what it should be
    # This does not work when processing user input :/
    if [[ ($GWT_TEST_ENV == false) || (-z $GWT_TEST_ENV) ]]; then
      eval gwt__reason="$2"
    else
      gwt__reason="$2"
    fi
    gwt__command="${gwt__command} --reason '${gwt__reason}'"
    shift
    shift

    if [[ -z $* ]]; then
      gwt__lock_cmd="gwt__not_enough_args_error gwt__lock_help_menu"
      return
    fi
  fi

  gwt__command="${gwt__command} ${gwt__worktree_dir}/$1"

  gwt__lock_cmd="$gwt__command"
}

gwt__move_cmd=""
gwt__move() {
  if [[ -z $* ]]; then
    gwt__move_cmd="gwt__not_enough_args_error gwt__move_help_menu"
    return
  fi

  gwt__command="git worktree move"

  if [[ ($* =~ "-f") || ($* =~ "--force") ]]; then
    gwt__command="${gwt__command} -f -f"
    shift

    if [[ -z $* ]]; then
    gwt__move_cmd="gwt__not_enough_args_error gwt__move_help_menu"
      return
    fi
  fi

  if [[ $# -lt 2 ]]; then
    gwt__move_cmd="gwt__not_enough_args_error gwt__move_help_menu"
    return
  fi

  gwt__command="${gwt__command} ${gwt__worktree_dir}/$1 ${gwt__worktree_dir}/$2"

  gwt__move_cmd="$gwt__command"
}

gwt__prune_cmd=""
gwt__prune() {
  gwt__command="git worktree prune"
  gwt__opts=""

  while [ $# -gt 0 ]; do
    case $1 in
      --dry-run|-n)
        gwt__opts="${gwt__opts} --dry-run"
        shift
        ;;
      --verbose|-v)
        gwt__opts="${gwt__opts} --verbose"
        shift
        ;;
      --expire)
        gwt__opts="${gwt__opts} --expire $2"
        shift
        shift
        ;;
      *)
        gwt__prune_cmd="gwt__unknown_args_error prune_help_menu $*"
        return
        ;;
    esac
  done

  gwt__command="${gwt__command} ${gwt__opts}"

  gwt__prune_cmd="$gwt__command"
}

gwt__remove_cmd=""
gwt__remove() {
  # Todo: Possibly allow passing multiple worktrees
  if [[ -z $* ]]; then
    gwt__remove_cmd="gwt__not_enough_args_error gwt__remove_help_menu"
    return
  fi

  gwt__command="git worktree remove"
  gwt__opts=""
  gwt__branch_delete_cmd=""

  while [[ $# -gt 0 ]]; do
    case $1 in
      --force|-f)
        gwt__opts="${gwt__opts} -f -f"
        shift

        if [[ -z $* ]]; then
          gwt__remove_cmd="gwt__not_enough_args_error gwt__remove_help_menu"
          return
        fi
        ;;
      --branch|-b)
        gwt__worktree_name="${*: -1}"
        if [[ 
          $gwt__worktree_name == "--branch" || 
          $gwt__worktree_name == "-b" || 
          $gwt__worktree_name == "--force" || 
          $gwt__worktree_name == "-f" 
        ]]; then
          gwt__remove_cmd="gwt__not_enough_args_error gwt__remove_help_menu"
          return
        fi

        gwt__worktree_info=$(git worktree list | grep "$gwt__worktree_name")

        if [[ -z "$gwt__worktree_name" ]]; then
          gwt__remove_cmd="gwt__not_enough_args_error gwt__remove_help_menu"
          return
        fi

        gwt__stripped=${gwt__worktree_info##*\[}
        gwt__worktree_branch=${gwt__stripped%%\]*}

        if ! [[ ($GWT_TEST_ENV == false) || (-z $GWT_TEST_ENV) ]]; then
          gwt__worktree_branch="foobar"
        fi

        gwt__branch_delete_cmd="git branch -D $gwt__worktree_branch"
        shift
        ;;
      *)
        if [[ ($# -lt 2) ]]; then
          break
        fi

        gwt__remove_cmd="gwt__unknown_args_error gwt__remove_help_menu $*"
        return
        ;;
      esac
  done

  if [[ "$1" = -* || "$2" = -* ]]; then
    gwt__remove_cmd="gwt__unknown_args_error gwt__remove_help_menu $*"
    return
  fi

  gwt__command="${gwt__command} ${gwt__opts} $*"

  if [[ -n $gwt__branch_delete_cmd ]]; then
    gwt__print_info "Branch '$gwt__worktree_branch' will be deleted. This cannot be undone. Are you sure? y/N"
    read -r gwt__confirm

    if [[ $gwt__confirm == "y" || $gwt__confirm == "yes" ]]; then
      gwt__command="${gwt__command} && ${gwt__branch_delete_cmd}"
    fi
  fi

  gwt__print_info "Removing worktree '$1'"

  gwt__remove_cmd="$gwt__command"
}

gwt__repair_cmd=""
gwt__repair() {
  gwt__repair_cmd="git worktree repair"
}

gwt__unlock_cmd=""
gwt__unlock() {
  if [[ -z $1 ]]; then
    gwt__unlock_cmd="gwt__not_enough_args_error gwt__unlock_help_menu"
    return
  fi

  gwt__unlock_cmd="git worktree unlock $1"
}

gwt__switch_cmd=""
gwt__switch() {
  if ! [[ $(git rev-parse --is-inside-work-tree) ]]; then
    gwt__switch_cmd="gwt__not_a_git_repo_error_message"
    return
  fi

  gwt__command=""
  if [[ (-z $1) || ($1 == "-") ]]; then
    gwt__main_tree=$(git worktree list --porcelain | awk '{print $0; exit}')
    gwt__main_dir="${gwt__main_tree//worktree /}"

    gwt__print_info "Changing to main worktree at: ${gwt__main_dir}"

    gwt__command="cd ${gwt__main_dir}"
    gwt__switch_cmd="$gwt__command"

    return
  fi

  gwt__worktree="${gwt__worktree_dir}/$1"
  gwt__print_info "Changing to worktree at: ${gwt__worktree}"

    gwt__command="cd ${gwt__worktree}"
  if [[ -z "$GWT_AUTO_INSTALL_DEPS" || "$GWT_AUTO_INSTALL_DEPS" == "true" ]]; then
    gwt__command="${gwt__command} && gwt__install_deps_if_needed"
  fi

  gwt__switch_cmd="$gwt__command"
}

gwt__version() {
  echo "$GWT_VERSION"
}

## Main ##

main() {
  gwt__check_for_worktree_dir

  case $1 in
    help|-h)
      shift
      gwt__help "$@"
      ;;
    add|-a)
      shift 
      gwt__add "$@"
      eval "$gwt__add_cmd"
      ;;
    list|-l)
      shift
      gwt__list "$@"
      # echo "list cmd: $cmd"
      eval "$gwt__list_cmd"
      ;;
    lock|-k)
      shift
      gwt__lock "$@"
      # echo "lock cmd: $cmd"
      eval "$gwt__lock_cmd"
      ;;
    move|-m)
      shift
      gwt__move "$@"
      # echo "move cmd: $cmd"
      eval "$gwt__move_cmd"
      ;;
    prune|-p)
      shift
      gwt__prune "$@"
      # echo "prune cmd: $cmd"
      eval "$gwt__prune_cmd"
      ;;
    remove|-r)
      shift
      gwt__remove "$@"
      # echo "remove cmd: $cmd"
      eval "$gwt__remove_cmd"
      ;;
    repair|fix|-f)
      shift
      gwt__repair "$@"
      # echo "repair cmd: $cmd"
      eval "$gwt__repair_cmd"
      ;;
    unlock|-u)
      shift
      gwt__unlock "$@"
      # echo "unlock cmd: $cmd"
      eval "$gwt__unlock_cmd"
      ;;
    switch|-s)
      shift
      gwt__switch "$@"
      eval "$gwt__switch_cmd"
      ;;
    version|-v)
      shift
      gwt__version
      ;;
    *)
      gwt__help "$@"
      echo ""
      ;;
  esac
}

if [[ ($GWT_TEST_ENV == false) || (-z $GWT_TEST_ENV) ]]; then
  main "$@"
fi
