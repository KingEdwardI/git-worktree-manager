#!/bin/bash
#shellcheck disable=SC2154

gwt__array_includes() {
  array="$1"
  value="$2"
  if [[ " ${array[*]} " =~ ${value} ]]; then
    echo true
  else
    echo false
  fi
}

gwt__check_for_worktree_dir() {
  if ! [[ (-d "${gwt__worktree_dir}")]]; then
    mkdir "${gwt__worktree_dir}"
  fi
}

gwt__not_enough_args_error() {
  gwt__help_message="$1"

  echo "Error: not enough arguments"
  echo "---------------------------"

  eval "$gwt__help_message"
}

gwt__unknown_args_error() {
  menu="$1"
  shift

  echo "Error: Unknown option(s) $*"
  echo "---------------------------"

  eval "$menu"
}

gwt__invalid_arg_combo_error_message() {
  echo "Fatal: Options '-b', '-B', and '--detach' cannot be used together"
}

gwt__invalid_use_of_reason_error_message() {
  echo "Fatal: The option '--reason' requires '--lock'"
}

gwt__not_a_git_repo_error_message() {
  echo "Fatal: Current directory is not a git repo"
}

gwt__print_info() {
 if [[ (-z $GWT_TEST_ENV) || ($GWT_TEST_ENV == false) ]]; then
   echo "$1"
 fi
}

gwt__install_deps_if_needed() {
  if [[ -a 'package.json' && -a 'package-lock.json' && ! -d 'node_modules' ]]; then
    npm ci
  elif [[ -a 'package.json' && ! -a 'package-lock.json' && ! -d 'node_modules' ]]; then
    npm i
  fi
}

